# nginx

Deve ser criado os seguintes diret�rios para persist�ncia de dados.

```
mkdir -p /srv/persist-services/nginx/conf.d && \
mkdir -p /srv/persist-services/nginx/logs && \
mkdir -p /srv/persist-services/security
```

### Criar o servi�o

para criar o servi�o deve ser executada a seguinte linha de comando:

```
docker stack deploy -c stack nginx
```

para verificar o status dos containers do servi�o deve ser executado a seguinte linha de comando:

```
docker service ps nginx_app
```


Para remover o servi�o do Nginx deve ser removido com o seguinte comando:

```
docker service rm nginx
```
